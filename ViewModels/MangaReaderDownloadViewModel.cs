﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MangaCenter.Models;
using System.Windows.Input;
using MangaCenter.Commands;

namespace MangaCenter.ViewModels
{
    public class MangaReaderDownloadViewModel : MangaDownloadViewModel
    {
        #region ICommands
        public ICommand DownloadCommand
        {
            get;
            private set;
        }
        #endregion

        public MangaReaderDownloadViewModel()
        {
            DownloadCommand = new DownloadMangaCommand(this);
        }

        public override void StartDownloading()
        {
            if (!initializationException)
            {
                Download();
            }
        }

        private void Download()
        {
            extractMangaName();
            while (extractMangaChapter())
            {                
                try
                {
                    //selecting div which holds image and link to next URL
                    HtmlNode imgDiv = document.DocumentNode.SelectSingleNode("//img[@id='img']");
                    string imgLink = extractImageLink(imgDiv);
                    string nextURL = extractNextPageLink(imgDiv);
                    string filename = String.Format(@"{0}\{1}\{2}{3}",
                        localMangaPath, currentChapterNumber, currentPageNumber, Path.GetExtension(imgLink));

                    downloadImage(imgLink, filename);

                    _mangaURL = nextURL;
                    initParser();

                    currentPageNumber++;
                }
                catch(NullReferenceException)
                {
                    continue;
                }
                
            }
            InfoString = "Manga has ended.\n" + InfoString;
        }

        private string extractImageLink(HtmlNode imgDiv)
        {
            string imgLink = imgDiv.Attributes["src"].Value.ToString();
            return imgLink;
        }

        private string extractNextPageLink(HtmlNode imgDiv)
        {
            string nextUrl = "http://www.mangareader.net" + imgDiv.ParentNode.Attributes["href"].Value.ToString();
            return nextUrl;
        }

        private bool extractMangaChapter()
        {
            
            try
            {
                var chapterDiv = document.DocumentNode.SelectSingleNode("//img[@id='img']");
                string altAtt = null;
                if (chapterDiv == null)
                    return false;
                else
                    altAtt = chapterDiv.Attributes["alt"].Value.ToString();

                string chapterNumberGarbage = altAtt.Remove(0, mangaName.Count());
                string[] tempString = chapterNumberGarbage.Split('-');

                currentChapterNumber = "Chapter " + tempString[0].Trim();
                createDirectory(currentChapterNumber);
                return true;
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("NullRefException");
                return extractMangaChapter();
            }
        }

        protected override void extractMangaName()
        {
            try
            {
                //getting manga name
                var nameDiv = document.DocumentNode.SelectNodes("//h2[@class='c2'] //a");
                mangaName = nameDiv.ElementAt(nameDiv.Count() - 1).InnerHtml;
                mangaName = mangaName.Remove(mangaName.Length - 6);
                validateMangaName();
                localMangaPath = DefaultOptions.Instance.DirectoryPath + @"\" + mangaName;
                //******************

                //creating manga directory
                createDirectory();
                //************************
                //creating txt file for link to manga
                TextWriter tw = new StreamWriter(localMangaPath + @"\" + mangaName + ".txt", false);
                tw.WriteLine(_mangaURL);
                tw.Close();
                //***********************************
                InfoString += "Manga Name: " + mangaName;
            }
            catch (NullReferenceException)
            {
                extractMangaChapter();
            }
        }

        public bool CanUpdate()
        {
            return !initParser();
        }
    }
}
