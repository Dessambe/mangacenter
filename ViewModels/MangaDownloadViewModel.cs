﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Windows;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using MangaCenter.Models;

namespace MangaCenter.ViewModels
{
    public abstract class MangaDownloadViewModel : BaseViewModel
    {
        protected HtmlWeb getHtmlWeb = new HtmlWeb();
        protected HtmlDocument document { get; set; }

        private string _libraryDirectory = DefaultOptions.Instance.DirectoryPath;
        public string LibraryDirectory
        {
            get { return _libraryDirectory; }
        }

        protected string localMangaPath { get; set; }
        public string MangaURL
        {
            get { return _mangaURL; }
            set { _mangaURL = value; OnPropertyChanged("MangaURL"); }
        }
        protected string _mangaURL { get; set; }

        protected string mangaName { get; set; }

        protected string currentChapterNumber { get; set; }
        protected int currentPageNumber { get; set; }

        private string _infoString;
        public string InfoString
        {
            get { return _infoString; }
            set { this._infoString = value; this.OnPropertyChanged("InfoString"); }
        }

        protected bool initializationException { get; set; }

        protected void downloadImage(string url, string filename)
        {
            using (WebClient wb = new WebClient())
            {
                wb.DownloadFile(url, filename);
                InfoString = String.Format("Page {0} of {1} chapter has been downloaded.\n{2}", 
                    currentPageNumber, currentChapterNumber, InfoString);
            }
        }

        protected bool initParser()
        {
            try
            {
                document = null;
                document = getHtmlWeb.Load(MangaURL);
                initializationException = false;
                return initializationException;
            }
            catch (Exception)
            {
                initializationException = true;
                return initializationException;
            }
            
        }

        protected void createDirectory(string concat = "")
        {
            if (!String.IsNullOrEmpty(concat))
            {
                concat = @"\" + concat;
            }
                

            if (!Directory.Exists(localMangaPath + concat))
            {
                currentPageNumber = 1; //when creating new chapter directory, page number resets itself to one.
                Directory.CreateDirectory(localMangaPath + concat);
            }
        }


        /// <summary>
        /// checking if the directory name is not containing invalid characters, if true - replacing them with empty character
        /// </summary>
        protected void validateMangaName()
        {
            Console.WriteLine("Przed zmiana: " + mangaName);
            Regex pattern = new Regex("[\\/:*?\"<>|]|[\n]{2}");

            mangaName = pattern.Replace(mangaName, " ");
        }

        public abstract void StartDownloading();
        protected abstract void extractMangaName();
    }
}
