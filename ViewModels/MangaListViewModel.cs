﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MangaCenter.Models;
using System.ComponentModel;
using System.IO;
using System.Collections.ObjectModel;
using System.Windows.Input;
using MangaCenter.Commands;
using MangaCenter.Views;

namespace MangaCenter.ViewModels
{
    public class MangaListViewModel
    {
        private string directory = DefaultOptions.Instance.DirectoryPath;
        private MangaDownloadViewModel mangaDownloadViewModel;        
        public ObservableCollection<Manga> Mangas { get; set; }

        public MangaListViewModel()
        {
            mangaDownloadViewModel = new MangaReaderDownloadViewModel();
            Mangas = new ObservableCollection<Manga>();
            RefreshList();

            RefreshCommand = new RefreshListCommand(this);
            DownloadWindowCommand = new ShowDownloadWindowCommand(this);
            OpenCommand = new OpenLibraryCommand(this);
            ReadCommand = new ReadMangaCommand(this);

        }

        public void OpenLibraryInExplorer()
        {
            System.Diagnostics.Process.Start("explorer.exe", directory);
        }

        public void RefreshList()
        {
            Mangas.Clear();
            string[] mangaList = Directory.GetDirectories(directory);
            foreach (string title in mangaList)
            {
                Manga manga = new Manga();
                manga.Name = Path.GetFileName(title);
                manga.Location = Path.GetFullPath(title);
                string[] listOfChapters = Directory.GetDirectories(title + @"\");
                manga.SetChapterList(listOfChapters);
                Mangas.Add(manga);
            }
        }

        public void DownloadWindow()
        {
            DownloadWindow window = new DownloadWindow();
            window.ShowDialog();
        }

        public void ReadMangaWindow()
        {
            ReadMangaWindow window = new ReadMangaWindow();
            window.ShowDialog();
        }

        #region ICommands
        public ICommand RefreshCommand
        {
            get;
            private set;
        }

        public ICommand DownloadWindowCommand
        {
            get;
            private set;
        }

        public ICommand OpenCommand
        {
            get;
            private set;
        }

        public ICommand ReadCommand
        {
            get;
            private set;
        }
        #endregion
    }
}
