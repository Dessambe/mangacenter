﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MangaCenter.Models;
using MangaCenter.ViewModels;
using System.Windows.Input;

namespace MangaCenter.Commands
{
    class ReadMangaCommand : ICommand
    {
        MangaListViewModel viewModel;

        public ReadMangaCommand(MangaListViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            Manga manga = parameter as Manga;
            Console.WriteLine(manga.Name);
            viewModel.ReadMangaWindow();
        }
    }
}
