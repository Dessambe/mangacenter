﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MangaCenter.ViewModels;
using System.ComponentModel;

namespace MangaCenter.Commands
{
    public class DownloadMangaCommand : ICommand
    {
        BackgroundWorker worker;
        private MangaReaderDownloadViewModel viewModel;

        public DownloadMangaCommand(MangaReaderDownloadViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return viewModel.CanUpdate();
        }

        public void Execute(object parameter)
        {
            worker = new BackgroundWorker();
            worker.DoWork += worker_DoWork;
            worker.RunWorkerAsync();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            viewModel.StartDownloading();
        }
    }
}
