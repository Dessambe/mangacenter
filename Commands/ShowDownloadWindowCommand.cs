﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MangaCenter.ViewModels;

namespace MangaCenter.Commands
{
    class ShowDownloadWindowCommand : ICommand
    {
        private MangaListViewModel viewModel;

        public ShowDownloadWindowCommand(MangaListViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            viewModel.DownloadWindow();
        }
    }
}
