﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MangaCenter.ViewModels;

namespace MangaCenter.Commands
{
    internal class RefreshListCommand : ICommand
    {
        private MangaListViewModel viewModel;

        public RefreshListCommand(MangaListViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            viewModel.RefreshList();
        }
    }
}
