﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MangaCenter.ViewModels;

namespace MangaCenter.Models
{
    public sealed class DefaultOptions : BaseViewModel
    {
        public static string _directoryPath { get; set; }
        public string DirectoryPath
        {
            get { return _directoryPath; }
            set { _directoryPath = value; OnPropertyChanged("DirectoryPath"); }
        }

        public DefaultOptions()
        {
            if(String.IsNullOrEmpty(DirectoryPath))
                DirectoryPath = Directory.GetCurrentDirectory() + @"\MangaLibrary";

            if (!Directory.Exists(DirectoryPath))
                Directory.CreateDirectory(DirectoryPath);

            Console.WriteLine("directory changed.");
        }

        private static DefaultOptions _instance = new DefaultOptions();
        public static DefaultOptions Instance { get { return _instance; } }
    }
}
