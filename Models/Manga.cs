﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MangaCenter.Models
{
    public class Manga
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public List<string> mangaChapters = new List<string>();
        public int NumberOfChapters { get { return mangaChapters.Count; } }

        public void SetChapterList(string[] listOfChapters)
        {
            foreach (string chapter in listOfChapters)
            {
                mangaChapters.Add(chapter);
            }
        }
    }
}
